<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?php 
	$current = 'technical';
?>

<head>
    <title>Technical Information ~ <?php  include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?php 
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?php 
	                include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?php 
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?php 
    	include('./menu.php');		
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Technical Information</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Technical Information</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->

                <h4>DATE AND PLACE:</h4>

<p><strong>September 10 – 15, 2013, Ostrava – Czech Republic</strong><br/>
<strong>September 12 – 13, 2013 – singles events</strong><br/>
<strong>September 13 – 14, 2013 – team events</strong> </p>

<p><strong>TJ Ostrava, Varenska street 40a, 702 00 Ostrava, Czech Republic</strong></p>

<hr />

<h4>EVENTS:</h4>

<p><strong>the following events will be played:</strong></p>

<p>Men’s singles (class 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 and 11)<br/>
Women’s singles (class 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 and 11)<br/>
Men’s team (class 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 and 11)<br/>
Women’s team (class 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 and 11) </p>

<p>Note 1: depending on the entries, the organisers and the Technical Delegate reserve the right to combine classes as may be necessary.<br/>
Note 2: the singles events will be played first followed by the team events.<br/>
Note 3: As there will not be classification for class 11, entries only from class 11 players who have a confirmed status will be accepted.</p>

<hr />

<h4>SCHEDULE:</h4>

<p>Arrival for classifications + classification: September 10th ,2013
<br />
<table>
<tr><td>Normal arrival + practice day:</td><td>September 11th. 2013</td></tr>
<tr><td>Technical meeting:</td><td>September 11th, 2013</td></tr>
<tr><td>Opening ceremony:</td><td>September 12th, 2013</td></tr>
<tr><td>Competition days:</td><td>September 12th – 14th, 2013</td></tr>
<tr><td>Closing ceremony:</td><td>September 14th, 2013</td></tr>
<tr><td>Departures:</td><td>September 15th, 2013</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>Proposed dates for the singles events:</td><td>September 12th – 13th, 2013</td></tr>
<tr><td>Proposed dates for the team events:</td><td>September 13th – 14th, 2013</td></tr>
</table>
</p>

<hr />

<h4>RULES:</h4>

<p>the event will be conducted in accordance with the current Laws of Table Tennis, the Regulations for International Competitions and specific PTT directives (which may be amended from time to time).</p>

<hr />

<h4>EQUIPMENT:</h4>

<p>all the used sport equipment is applied following ITTF and ITTF PTT Rules</p>
<br />
<table>
<tr><td>Tables:&nbsp;&nbsp;</td><td>24 ANDRO blue, from the number are 16 tables accessible for the wheelchairs players</td></tr>
<tr><td>Nets:</td><td>Andro</td></tr>
<tr><td>Balls:</td><td>Yasaka, white 3 stars</td></tr>
<tr><td>Floor:</td><td>wooden in the hall for the standing and blue rubber in the hall for the wheelchairs</td></tr>
</table>

<hr />

<h4>ELIGIBILITY</h4>

<p>The event is open to players who are eligible to represent their national association according to the ITTF Handbook 2010&#8211;2011.</p>

<p>AAAAAAAAAAA and the classifier is BBBBBBBBBB (Fa20).</p>

<hr />

<h4>CLASSIFICATION:</h4>

<p>All new players and players who are to have their classification reviewed should be present in time for classification. This means that all players should arrive by September 10th i.e.</p>

<p>the day before the classification is scheduled. This is very important as players not present on time may not be classified.</p>

<p>All players should bring with them their medical diagnosis and any other medical information relevant to their classification. They should report to classification with these documents, dressed as if they are going to play a match and with all their table tennis equipment including sports chairs. All players and support staff are expected to cooperate fully in the classification process.</p>

<hr />

<h4>PARTICIPATION QUOTAS:</h4>

<p>A player may only play in the class indicated on his or her international classification card unless, due to the low number of players, his or her class is combined with the next class or classes. Then they may play in the next higher class event.</p>

<p>The minimum entry for a singles class to be played is 4 players.<br/>
For Fa20 events, the maximum number of entries per association per class is 6 plus 2 extra juniors.
For Fa40 events, the maximum number of entries per association per class is 4 but the host association may enter up to 6 players.</p>

<p>The minimum entry for a team event to be played is 4 teams with 2, 3 or 4 players forming a team.
In Fa40 events, each Association may enter a maximum of 2 teams but the host may enter 2 teams per class.
In Fa20 events, a maximum 2 teams per Association may be entered where all players are from the same Association.
Players from different countries may form a team in the team event in Fa40 and Fa20 competitions, but if there are 3 players in the same event from the same Association, only the 3rd lowest ranked player may form a team with a player from another Association.</p>

<p>All other persons wishing to accompany a team (i.e. who are not members of the team) are subject to special charges and should contact the organisers for further information. These packages are limited and subject to availability of places in the official hotels. Priority will be given to the Official Party of all the delegations.</p>


                <hr/>

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <!-- start: Footer -->
    <?php 
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
