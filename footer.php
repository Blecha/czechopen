<!-- start: Footer -->
<footer id="footer">
    <div class="wrapper">

        <div class="row-fluid">
            <div class="span12">
                <h2>The address of the Organizing Committee:</h2>
                <address>
                    <p>
                        <i class="icon-map-marker"></i> Varenska street 3098/40a, 702 00 Ostrava, Czech Republic<br/>
                        <br />
                        <i class=" iconm-user-5"></i> Mr. Petr Stanicek<br />
                        <i class="iconm-mail-3"></i> <a href="czechopenostrava@gmail.com">czechopenostrava@gmail.com</a><br />
                        <i class="iconm-mobile-2"></i><a href="tel:+420733693891"> (+420) 733 693 891</a><br />
                    </p>
                </address>
            </div>
        </div>

        <!-- Footer Menu -->
        <section id="footer-menu">

            <div class="row-fluid">
                <div class="span4">
                    <p class="copyright">&copy; Copyright 2018 <a href="http://sepiri.cz">Sepiri</a>.</p>
                </div>
                <div class="span8 hidden-phone">
                    <ul class="pull-right">
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </div>
            </div>

        </section>
        <!-- end: Footer Menu -->

    </div>
</footer>
<!-- end: Footer -->