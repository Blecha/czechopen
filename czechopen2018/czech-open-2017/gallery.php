<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?
	$current = 'gallery';
?>

<head>
    <title>Gallery ~ <? include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?
    	include('./menu.php');
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Gallery</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Gallery</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->

                <!-- start: Filter-->
               <div class="row-fluid">
                     <!--<div class="span12">
                        <ul id="filtrable" class="clearfix">
                            <li class="current all"><a href="#all">All</a></li>
                            <li class="html"><a href="#html">Html</a></li>
                            <li class="css"><a href="#css">CSS</a></li>
                            <li class="php"><a href="#php">PHP</a></li>
                            <li class="javascript"><a href="#javascript">JavaScript</a></li>
                        </ul>
                    </div>-->
                </div>
                <!-- end: Filter-->

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <!-- start: Portfolio -->
    <section class="row portfolio filtrable clearfix">

	    <? for($i = 1; $i <= 179; $i++){ ?>
        <article data-id="id-1" data-type="javascript html" class="span3">
            <div class="inner-image">
                <a href="images/gallery/<? echo $i;?>.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                    <img src="images/gallery/<? echo $i;?>.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                    <span class="frame-overlay"></span>
                </a>
            </div>
            <div class="sliding">
                <div class="inner-text">
                    <h4 class="title"><a href="#">Czech Open 2017</a></h4>
                </div>
            </div>
        </article>
        <? } ?>
    </section>
    <!-- end: Portfolio -->

    <!-- start: Footer -->
    <?
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
