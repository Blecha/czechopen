<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?
	$current = 'resultlist';
?>

<head>
    <title>Results list ~ <? include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?
    	include('./menu.php');
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Results list</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Results list</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->
                <!--
								<div class="row-fluid">
                    <div class="span12">
                    	  <p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tf2.pdf"><h3>TF2</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tf3.pdf"><h3>TF3</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tf4-5.pdf"><h3>TF4-5</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tf6-10.pdf"><h3>TF6-10</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm1-2.pdf"><h3>TM1-2</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm3.pdf"><h3>TM3</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm4.pdf"><h3>TM4</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm5.pdf"><h3>TM5</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm6-7.pdf"><h3>TM6-7</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm8.pdf"><h3>TM8</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tm9-10.pdf"><h3>TM9-10</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/tfm11.pdf"><h3>TFM11</h3></a>
                        </p>
                    </div>
                </div>

								<hr />

                <div class="row-fluid">
                    <div class="span12">
                    	  <p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf1-2.pdf"><h3>Class SF1-2</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf3.pdf"><h3>Class SF3</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf4-5.pdf"><h3>Class SF4-5</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf6.pdf"><h3>Class SF6</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf7-10.pdf"><h3>Class SF7-10</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sf11.pdf"><h3>Class SF11</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm1.pdf"><h3>Class SM1</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm2.pdf"><h3>Class SM2</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm3.pdf"><h3>Class SM3</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm4.pdf"><h3>Class SM4</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm5.pdf"><h3>Class SM5</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm6.pdf"><h3>Class SM6</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm7.pdf"><h3>Class SM7</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm8.pdf"><h3>Class SM8</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm9.pdf"><h3>Class SM9</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm10.pdf"><h3>Class SM10</h3></a>
                        </p>
												<p><br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2015/results/class-sm11.pdf"><h3>Class SM11</h3></a>
                        </p>
												<hr />
                      
                    </div>
                </div>
                -->

                <hr/>

                <!-- live broadcast -->
	            <div class="hero-unit">
	                <h1 style="font-size:53px" align="center">Online broadcasting of matches</h1>
	                <p></p>
	                <br />
	                <p align="center"><a href="http://ostatni.tvcom.cz/" class="btn btn-primary btn-large"><img src="images/logo-over.png"/></a></p>
	            </div>

            </div>
            <!-- end: Wrapper -->


        </section>
        <!-- end: Page section -->



    </div>

    <!-- start: Footer -->
    <?
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
