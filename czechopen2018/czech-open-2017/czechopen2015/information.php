<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?
	$current = 'information';
?>

<head>
    <title>Information ~ <? include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?
    	include('./menu.php');		
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Information</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Information</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->

                <div class="row-fluid pull-center">
                    <div class="span12 service">
                        <div class="hero-unit">
                            <h2>Competition venue</h2>
                            <p>
	                            Telovychovna jednota Ostrava, Varenska street 3098/40a, 702 00 Ostrava, Czech Republic
                            </p>
                            <div class="gmap"><iframe class="" width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=cs&amp;geocode=&amp;q=TJ+Ostrava,+Varensk%C3%A1+40+a,+702+00+Ostrava&amp;sll=37.0625,-95.677068&amp;sspn=50.244827,93.076172&amp;t=m&amp;ie=UTF8&amp;hq=TJ&amp;hnear=Varensk%C3%A1+3050%2F40,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;ll=49.835054,18.269201&amp;spn=0.006295,0.006295&amp;output=embed"></iframe>
                        </div>
                        	<p>    
								Sports hall of TJ Ostrava about 1200 m from the hotel
<br /><br />

							<strong>OFFICIAL HOTEL</strong><br /> 
							The hotel for teams is:
							<br />
							<center>	 
							<table>
								<tr>
									<td>Name:</td><td>Park Inn by Radisson Ostrava - 4 stars</td>
								</tr>
								<tr>
									<td>Physical/ postal address:&nbsp;&nbsp;&nbsp;</td><td>Hornopolni street 3313/42, Ostrava</td>
								</tr>
								<tr>
									<td>E-mail:</td><td><a href="mailto:rezervace.ostrava@rezidoparkinn.com">rezervace.ostrava@rezidoparkinn.com</a></td>
								</tr>
								<tr>
									<td>Phone:</td><td>+420 595 195 509</td>
								</tr>
								<tr>
									<td>Fax:</td><td>+420 595 195 555</td>
								</tr>
								<tr>
									<td>Website address:</td><td><a href="http://www.parkinn.cz/hotel-ostrava" target="_blank">www.parkinn.com/hotel-ostrava</a></td>
								</tr>
							</table>
							</center>	
							<br />
							<div class="gmap"><iframe class="2" width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=cs&amp;q=Park+Inn+Ostrava+Hornopoln%C3%AD+3313%2F42,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;ie=UTF8&amp;geocode=FUmI-AIdw8EWAQ&amp;split=0&amp;hq=&amp;hnear=Park+Inn+Ostrava+Hornopoln%C3%AD+3313%2F42,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;t=m&amp;z=14&amp;ll=49.842249,18.268611&amp;output=embed"></iframe>
							</div>
                            </p>
                            
                            <p>    
							Sports hall of TJ Ostrava about 1200 m from the hotel
<br /><br />

							<strong>Way to the hotel</strong>
							<br /> 
							<br />
							<div class="gmap"><iframe class="2" width="100%" height="650" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=Varensk%C3%A1+40a,+702+00+Moravsk%C3%A1+Ostrava&amp;daddr=Hornopolni+street+3313%2F42,+Ostrava&amp;hl=cs&amp;geocode=FfBs-AId1sIWASltIJ7RFeMTRzHu1kwdQmip6A%3BFUmI-AIdxMEWASnTIysfGuMTRzE_g8_FwVO5Ow&amp;aq=&amp;sll=49.835248,18.268886&amp;sspn=0.010075,0.022724&amp;mra=ls&amp;ie=UTF8&amp;ll=49.835242,18.26889&amp;spn=0.008122,0.007193&amp;t=m&amp;output=embed"></iframe>
							</div>
                            </p>

                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row-fluid">
                    <div class="span6 service">
                        <div class="well">
                            <h2>TRANSPORTATION</h2>
                            <div class="service-desc">
								<p>
									The organizers will provide transportation for teams and officials from the airport Ostrava or from the Main Railway station Ostrava to hotel Park Inn and back and local transport from hotel to Venue and back. We will use for it the right number of buses and minibuses.
								</p>
								<p>
Ostrava has direct train connection with the capital Prague and other cities abroad (Wien, Warszawa, Kosice, etc.). There are two main railway stations: Ostrava-Hlavni nadrazi (old) and Ostrava-Svinov (new). Near Ostrava is also Leos Janacek Airport Ostrava is serving a number of domestic and international destinations (Prague, London, and Paris).
								</p>									
								<img src="images/czechopen_map_airports.jpg" />
								<br /><br />
								<strong><u>Wheelchair availability</u></strong>
								<p>
									Be aware that most of transport is on seať. Wheelchair must be stored in the luggage space. For more information please contact <a href="mailto:stanicekp@seznam.cz">Transport Officer</a>.
								</p>
								<strong><u>Free pick up/drop off transport service</u></strong>
								<p>
								From the airport in Ostrava and the railway station Ostrava-Svinov the organizer provides transport to/from the hotel for free.
								<br /><br />
								<strong>Ostrava: Leoš Janáček Ostrava Airport, Czech Republic</strong><br />
								Distance: 25km – Bus (45min)<br />
								Web: <a href="http://www.airport-ostrava.cz/" target="_blank">http://www.airport-ostrava.cz/</a><br />
								Price: for Free
								<br /><br />
								<strong>Ostrava-Svinov Railway Station, Czech Republic</strong><br />
								Distance: 5km – Bus (15 min) <br />
								Price: for Free <br />
								<br />
								<strong><u>Paid pick up/drop off transport service</u></strong>
								<p>
								There is also possibility to order paid transport from farther airports in Katowice (POL), Krakow (POL), Vienna (AUT) and Prague (CZE), operated by TigerExpress transport company. Prague is cooperated with one of the train companies. Web: <a href="http://www.tigerexpress.eu/" target="_blank">http://www.tigerexpress.eu/</a> 
								</p>
								<strong>Katowice:</strong> Katowice International Airport, Poland <br />
								Distance: 120km – Bus (1,5h) <br />
								Web: <a href="http://www.katowice-airport.com/" target="_blank">http://www.katowice-airport.com/</a> <br />
								Price: one way ticket 13€ <br />
								<br />
								<strong>Krakow:</strong> John Paul II International Airport Kraków-Balice, Poland <br />
								Distance: 150km – Bus (2h) <br />
								Web: <a href="http://www.krakowairport.pl/" target="_blank">http://www.krakowairport.pl/</a> <br />
								Price: one way ticket 20€ <br />
								<br />
								<strong>Vienna: </strong> Vienna International Airport, Austria <br />
								Distance: 310km – Bus (3h), Train (4h) <br />
								Web: <a href="http://www.viennaairport.com/" target="_blank">http://www.viennaairport.com/</a> <br />
								Price: one way ticket 20€ <br />
								<br />
								<p>
								From Katowice, Krakow and Vienna airports we recommend airport shuttle service to/from the hotel by minibuses operated by local transport company ‘TigerExpress.eu’. It provides top quality transfer service between the Airports and the Park Inn Hotel in Ostrava. TigerExpress staff speaks English and other languages and is ready to provide you with the full assistance during the journey.
								</p>
								<p>
								For reservation ‘TigerExpress’ airport shuttle please contact <a href="mailto:stanicekp@seznam.cz">Transport Officer</a>
								</p>
								
								<strong>Prague:</strong> Vaclav Havel Airport Prague, Czech Republic <br />								
								Distance: 390km – Train (3,5h) <br />
								Web: <a href="http://www.prg.aero/" target="_blank">http://www.prg.aero/</a> <br />
								Price: one way ticket 20€ <br />
								<br />
								<p>
								TigerExpress also provides a transport from Prague operated by one of the train companies. They will provide also transport from airport to railway station. English speaking staff will be available during the journey. Journey time is about 30min from airport to railway station and 3 hours by train.
								</p>
								<p>
								For reservation ‘TigerExpress’ transport please contact <a href="mailto:stanicekp@seznam.cz">Transport Officer</a>
								</p>
								<br />
								<strong><u>Other destinations possibilities</u></strong>
								<br /> <br />
								<p>
								From the following other airports organizer does not provide transportation (in exceptional cases only – by appointment).
								</p>
								<strong>Brno:</strong> Brno-Tuřany Airport, Czech Republic <br />
								Distance: 170km – Bus (2h), Train (3h) <br />
								Web: <a href="http://www.brno-airport.cz/" target="_blank">http://www.brno-airport.cz/</a> <br />
								<strong>Wroclaw:</strong> Copernicus Airport, Poland <br />
								Distance: 250km – Bus (2,5h), Train (5,5h) <br />
								Web: <a href="http://airport.wroclaw.pl/" target="_blank">http://airport.wroclaw.pl/</a> <br />
								<strong>Bratislava:</strong> Letisko M.R.Štefánika - Airport Bratislava, Slovakia <br />
								Distance: 305km – Train (3,5h) <br />
								Web: <a href="http://www.bts.aero/" target="_blank">http://www.bts.aero/</a> <br />
								<strong>Warsaw:</strong> Warsaw Chopin Airport, Poland <br />
								Distance: 390km – Train (5h) <br />
								Web: <a href="http://www.lotnisko-chopina.pl/" target="_blank">http://www.lotnisko-chopina.pl/</a> <br />
								<br />
								<strong>Contact person - Transport Officer:</strong> <br />
								Mr. Petr Staníček, <a href="mailto:stanicekp@seznam.cz">stanicekp@seznam.cz</a>, +420 733 693 891
								</p>
                            </div>
                        </div>
                    </div>
                    <div class="span6 service">
                        <div class="well">
                            <h2>VISA</h2>
                            <div class="service-icon"><i class="icon-cogs"></i></div>
                            <div class="service-desc">
                                <p>
                                    Should you need assistance to apply for a visa (e.g. a letter of invitation), please provide the organisers with the following details:
                                </p>
                                <ul class="the-icons">
                                    <li><i class="iconm-arrow-right-7"></i> Full name as in passport</li>
                                    <li><i class="iconm-arrow-right-7"></i> Function in the team</li>
                                    <li><i class="iconm-arrow-right-7"></i> Passport number</li>
                                    <li><i class="iconm-arrow-right-7"></i> Passport expiry date</li>
                                </ul>
                                <p>
	                                Note: the requirements for visas are not under the control of the ITTF or the organisers but under the Slovenian Government’s jurisdiction and the association must fulfil all requirements in order to get a visa in time. It may take more than a month to obtain a visa.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>
                
                <div class="row-fluid">
                    <div class="span6 service">
                        <div class="well">
                            <h2>ENTRY FEES</h2>
                            <div class="service-icon"><i class="icon-cog"></i></div>
                            <div class="service-desc">
                                <p>
                                	For PTT events, the entry fees include accommodation and capitation fees.  The entry fees have to be paid in Euro to the 									organisers as set out below:
                                </p>
                                <p>
                                	Entry to be submitted with the first entry = € 190 per person (2 in the room)<br /><br />
									“ 2nd “ = € 370 per person ( 2 in the room) + single supplement of € 35 per person/day. For extra days at <br />€ 75 per person/day.<br /><br />
									Payments should be made as follows:
                                </p>
                                <table>
                                	<tr>
                                		<td>Name of organisation:</td><td><strong>TJ Ostrava</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Bank name:</td><td><strong>Komercni banka a.s.</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Account name:</td><td><strong>TJ Ostrava</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Account number:</td><td><strong>61830761/0100</strong></td>
                                	</tr>
                                	<tr>
                                		<td>SWIFT:</td><td><strong>KOMBCZPP</strong></td>
                                	</tr>
                                	<tr>
                                		<td>IBAN:</td><td><strong>CZ8201000000000061830761</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Specification for Payment:</td><td><strong>“Czech Open 2015”</strong></td>
                                	</tr>
                                </table>
                                <h4 style="color:red;">PLEASE,  NOTE THAT BANK CHARGES MUST BE  INCLUDED IN THE ENTRY FEES !</h4>
                            </div>
                        </div>
                    </div>
                    <div class="span6 service">
                        <div class="well">
                            <h2>CANCELLATION POLICY</h2>
                            <div class="service-icon"><i class="icon-cogs"></i></div>
                            <div class="service-desc">
                                <p>
                                    The policy applies as follows:<br /><br />
									25.1	cancellation after the first entry but before the second entry:  the first entry fee is forfeited.<br /><br />
									25.2	cancellations after the second entry:  the first entry fee plus an additional 30% of the entry fee is forfeited i.e. a total of 60% of the total entry fee.<br /><br />
									25.3	cancellations within 10 calendar days of the arrival date will be decided by the organisers in consultation with the TD.<br />
									<br />
									This is provided that the player is not able to prove circumstances beyond his or her control e.g. admission to hospital.
									<br />
									<br />
									<br />
									<br />
									<br />
									<br />
									<br />
									<br />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>                

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <!-- start: Footer -->
    <?
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
