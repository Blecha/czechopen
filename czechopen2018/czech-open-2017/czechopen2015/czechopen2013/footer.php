<!-- start: Footer -->
<footer id="footer">
    <div class="wrapper">

        <div class="row-fluid">
            <div class="span12">
                <h2>The address of the Organizing Committee:</h2>
                <address>
                    <p>
                        <i class="icon-map-marker"></i> Varenská 40a, 702 00 Moravská Ostrava<br/>
                        <br />
                        <i class=" iconm-user-5"></i> Mr. Jiri Danek<br />
                        <i class="iconm-mail-3"></i> <a href="mailto:danekj@yasaka.cz">danekj@yasaka.cz</a><br />
                        <i class="icon-phone"></i> (+420) 596 635 275 (fax)<br/>
                        <i class="iconm-mobile-2"></i> (+420) 602 755 143<br />
                    </p>
                </address>
            </div>
        </div>

        <!-- Footer Menu -->
        <section id="footer-menu">

            <div class="row-fluid">
                <div class="span4">
                    <p class="copyright">&copy; Copyright 2013 <a href="http://sepiri.cz">Sepiri</a>.</p>
                </div>
                <div class="span8 hidden-phone">
                    <ul class="pull-right">
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </div>
            </div>

        </section>
        <!-- end: Footer Menu -->

    </div>
</footer>
<!-- end: Footer -->