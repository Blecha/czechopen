<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?
	$current = 'sponsors';
?>

<head>
    <title>Partners ~ <? include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?
    	include('./menu.php');
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Partners</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Partners</div>
                        </div>
                    </div>
                </section>

                <div class="row-fluid">
                	<h2>Tournament organizers</h2>
	                <ul class="thumbnails pull-center">
										<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.ipttc.org" target="_blank"><img src="images/sponsors/ittf.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.ping-pong.cz" target="_blank"><img src="images/sponsors/cast.png" /></a></div>
	                    </li>
	                	<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.tjostrava.cz" target="_blank"><img src="images/sponsors/tj-ostrava.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

                <hr/>

                <div class="row-fluid">
                	<h2>Under the auspices of</h2>
	                <ul class="thumbnails pull-center">
											<li class="span6 thumbnail">
												<img src="images/sponsors/zastita.jpg" />
											</li>
											<li class="span6 thumbnail">
												<img src="images/sponsors/zastita-msk.jpg" />
											</li>
	                </ul>
	            </div>

	            <hr />

	            <div class="row-fluid">
                	<h2>Main partners</h2>
	                <ul class="thumbnails pull-center">
	                	<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.kr-moravskoslezsky.cz" target="_blank"><img src="images/sponsors/msk.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.ostrava.cz" target="_blank"><img src="images/sponsors/ostrava.png" /></a></div>
	                    </li>
	                    <!--<li class="span3 thumbnail">
	                    	<div class="image-icon"><a href="http://www.cez.cz" target="_blank"><img src="images/sponsors/cez.png" /></a></div>
	                    </li>-->
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.parkinn.cz/hotel-ostrava" target="_blank"><img src="images/sponsors/park-inn.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

	            <hr />

	            <div class="row-fluid">
                	<h2>Partners</h2>
	                <ul class="thumbnails pull-center">
	                	<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.moap.cz" target="_blank"><img src="images/sponsors/moravska-ostrava.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                    	<div class="image-icon"><a href="http://corporate.arcelormittal.com" target="_blank"><img src="images/sponsors/arcelormittal.png" /></a></div>
	                        <!--<div class="image-icon"><a href="http://www.osu.cz" target="_blank"><img src="images/sponsors/univerzita.png" /></a></div>-->
	                    </li>
											<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.cechymen.cz" target="_blank"><img src="images/sponsors/cechymen.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

							<div class="row-fluid">
	                <ul class="thumbnails pull-center">
	                    <li class="span3 thumbnail">
	                    	<div class="image-icon"><a href="http://www.skvostrava.cz" target="_blank"><img src="images/sponsors/skv-ostrava.png" /></a></div>
	                        <!--<div class="image-icon"><a href="http://www.osu.cz" target="_blank"><img src="images/sponsors/univerzita.png" /></a></div>-->
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="#" target="_blank"><img src="images/sponsors/servisni-centrum-sportu.png" /></a></div>
	                    </li>
											<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.harmonyclub.cz" target="_blank"><img src="images/sponsors/harmony.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

	            <hr />

	            <div class="row-fluid">
                	<h2>For participation</h2>
	                <ul class="thumbnails pull-center">
	                	<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.ssss.cz" target="_blank"><img src="images/sponsors/stredni-skola-stravovani.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://escvsb.cz" target="_blank"><img src="images/sponsors/esc-vsb.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <!--<div class="image-icon"><a href="http://www.autoheller.cz" target="_blank"><img src="images/sponsors/auto-heller.png" /></a></div>-->
	                        <div class="image-icon"><a href="http://www.zsgepiky.cz" target="_blank"><img src="images/sponsors/zs-gen-piky.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.osu.cz" target="_blank"><img src="images/sponsors/univerzita.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

	            <div class="row-fluid">
	                <ul class="thumbnails pull-center">
	                	<li class="span3 thumbnail">
	                        <!--<div class="image-icon"><a href="http://ostravamestosportu.cz" target="_blank"><img src="images/sponsors/oecs.png" /></a></div>-->
	                        <div class="image-icon"><a href="http://www.dpo.cz" target="_blank"><img src="images/sponsors/dpo.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.tigerexpress.eu" target="_blank"><img src="images/sponsors/tiger-express.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.bosscan.org" target="_blank"><img src="images/sponsors/reproservis.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.acsnet.cz" target="_blank"><img src="images/sponsors/acs.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

	            <div class="row-fluid">
	                <ul class="thumbnails pull-center">
	                	<!--<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.barton-a-partner.cz" target="_blank"><img src="images/sponsors/barton.png" /></a></div>
	                    </li>-->
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.vsb.cz" target="_blank"><img src="images/sponsors/vsb.png" /></a></div>
	                    </li>
	                    <li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://milujivodu.cz" target="_blank"><img src="images/sponsors/miluji.png" /></a></div>
	                    </li>
	                    <!--<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.pekariaspol.cz" target="_blank"><img src="images/sponsors/pekari-a-spol.png" /></a></div>
	                    </li>-->
											<li class="span3 thumbnail">
		                        <div class="image-icon"><a href="http://www.pincesobchod.cz" target="_blank"><img src="images/sponsors/desaka.png" /></a></div>
		                  </li>
											<li class="span3 thumbnail">
		                        <div class="image-icon"><a href="http://www.hotelvista.cz" target="_blank"><img src="images/sponsors/vista.png" /></a></div>
		                  </li>
	                </ul>
	            </div>

	            <div class="row-fluid">
	                <ul class="thumbnails pull-center">
	                	  <!--<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.lahudkysixta.cz" target="_blank"><img src="images/sponsors/lahudky-sixta.png" /></a></div>
	                    </li>-->
	                    <!--<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.rec21.cz" target="_blank"><img src="images/sponsors/rec21.png" /></a></div>
	                    </li>-->
	                    <!--<li class="span3 thumbnail">
	                        <div class="image-icon"><a href="http://www.dankar.cz" target="_blank"><img src="images/sponsors/dankar.png" /></a></div>
	                    </li>-->
	                </ul>
	            </div>

							<div class="row-fluid">
	                <ul class="thumbnails pull-center">
	                	<li class="span3 thumbnail">
	                        <!--<div class="image-icon"><a href="http://ostravamestosportu.cz" target="_blank"><img src="images/sponsors/oecs.png" /></a></div>-->
	                        <div class="image-icon"><a href="http://www.lc-hotel.cz" target="_blank"><img src="images/sponsors/lowcost-hotel.png" /></a></div>
	                    </li>
											<li class="span3 thumbnail">
	                        <!--<div class="image-icon"><a href="http://ostravamestosportu.cz" target="_blank"><img src="images/sponsors/oecs.png" /></a></div>-->
	                        <div class="image-icon"><a href="http://www.vozickari-ostrava.cz/" target="_blank"><img src="images/sponsors/oov.png" /></a></div>
	                    </li>
											<li class="span3 thumbnail">
	                        <!--<div class="image-icon"><a href="http://ostravamestosportu.cz" target="_blank"><img src="images/sponsors/oecs.png" /></a></div>-->
	                        <div class="image-icon"><a href="http://www.osu.cz/" target="_blank"><img src="images/sponsors/osu.png" /></a></div>
	                    </li>
	                </ul>
	            </div>

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <!-- start: Footer -->
    <?
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
