<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?
	$current = 'index';
?>

<head>
    <title><? include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/layerslider.css" >

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script>

    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-25231006-44', 'czechopenostrava.com');
	  ga('send', 'pageview');

	</script>
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?
    	include('./menu.php');
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Slider -->
<section id="slider">
    <div class="container">
        <div id="layerslider-container">

            <div id="layerslider" style="width: 960px; height: 480px; margin-bottom: 80px;">

                <div class="ls-layer" style="slidedelay: 7000;">

                    <img src="example/layerslider/tn_1_1.jpg" class="ls-tn">
                    <img src="example/layerslider/slider-a1.jpg" class="ls-bg">
                    <img class="ls-s4" id="sss" src="example/layerslider/l13.png" style="top: -101px; left: 105px; slidedirection : left;">
                    <h1 class="ls-s5 l1-s1" style="top:200px; left: 303px; slidedirection : top; font-size: 80px;">
                        Czech Open 2015
                    </h1>
                    <h1 class="ls-s2 l1-s1b" style="top:200px; left: 303px; font-size: 80px; slidedirection : fade; slideoutdirection : fade;durationin : 1700; durationout : 2700; easingin : easeInOutQuad; easingout : easeInOutQuad; delayin : 1500; showuntil : 1600;">
                        Czech Open 2015
                    </h1>
                    <img class="ls-s7" src="example/layerslider/l16.png" style="top: 234px; left: -21px; slidedirection : bottom;">
                    <h2  class="ls-s2 l1-s3" style="top:330px; left: 400px; slidedirection : bottom; slideoutdirection : right; durationin : 2000; durationout : 1000; easingin : easeOutElastic; easingout : easeInOutQuint; delayin : 1000; showuntil : 2000;">
                        The 11th Czech Open
                    </h2>
                    <h2  class="ls-s3 l1-s4" style="top:373px; left: 320px; slidedirection : top; slideoutdirection : bottom; durationin : 1500; durationout : 1000; easingin : easeOutBack; easingout : easeInOutQuint; delayin : 1000; showuntil : 2000;">
                        of the Table Tennis of the Physically Handicaped
                    </h2>
                    <h2  class="ls-s3 l1-s4" style="top:330px; left: 50%; slidedirection : left; slideoutdirection : right; durationin : 1200; durationout : 1200; easingin : easeInOutQuart; easingout : easeInOutQuart; delayin : 5000;">
                        on the September 22th to 27th, 2015
                    </h2>
                    <h2  class="ls-s3 l1-s3" style="top:373px; left: 50%; slidedirection : right; slideoutdirection : left; durationin : 1200; durationout : 1200; easingin : easeInOutQuart; easingout : easeInOutQuint; delayin : 5000;">
                        Ostrava, Czech Republic
                    </h2>
                    <img class="ls-s1" src="example/layerslider/l16.png"  style="top: 313px; left: 728px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 8000; durationout : 1500; easingin : easeOutQuart; easingout : easeInOutQuint; delayin : 2000; ">
                    <h1  class="ls-s2 l1-s2" style="top:150px; left: 662px; slidedirection : top;  durationin : 2000; durationout : 1500; easingin : easeOutElastic; easingout : easeInOutQuint; delayin : 1500;">

                    </h1>
                    <h1  class="ls-s2 l1-s2b" style="top:150px; left: 662px; slidedirection : fade; slideoutdirection : fade; durationin : 1700; durationout : 2700; easingin : easeInOutQuad; easingout : easeInOutQuad; delayin : 3500; showuntil : 600;">

                    </h1>
                    <h2 class="ls-s2 l1-s2" style="top:150px; left: 750px; slidedirection : top;  durationin : 2300; durationout : 1500; easingin : easeOutElastic; easingout : easeInOutQuint; delayin : 1500;">

                    </h2>
                    <h2  class="ls-s2 l1-s2b" style="top:150px; left: 750px; slidedirection : fade; slideoutdirection : fade; durationin : 2000; durationout : 2700; easingin : easeInOutQuad; easingout : easeInOutQuad; delayin : 3500; showuntil : 600;">

                    </h2>


                </div>

                <div class="ls-layer layer2" style="slidedirection: right; slidedelay: 6500;">

                    <img src="example/layerslider/slider-a2.jpg" class="ls-bg">
                    <img src="example/layerslider/tn_1_2.jpg" class="ls-tn">
                    <img class="ls-s3"  src="example/layerslider/nf1.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : top; slideoutdirection : left;  delayin : 500; showuntil : 3100; ">
                    <img class="ls-s3"  src="example/layerslider/nf2.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : right; slideoutdirection : left;  delayin : 600; showuntil : 2500; ">
                    <img class="ls-s3"  src="example/layerslider/nf3.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : bottom; slideoutdirection : left;  delayin : 700; showuntil : 2600; ">
                    <img class="ls-s3"  src="example/layerslider/nf4.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : left; slideoutdirection : left;  delayin : 800; showuntil : 2900; ">
                    <img class="ls-s3"  src="example/layerslider/nf5.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : top; slideoutdirection : left;  delayin : 900; showuntil : 2900; ">
                    <img class="ls-s3"  src="example/layerslider/nf6.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : right; slideoutdirection : left;  delayin : 1000; showuntil : 2000; ">
                    <img class="ls-s3"  src="example/layerslider/nf7.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : bottom; slideoutdirection : left;  delayin : 1100; showuntil : 2400; ">
                    <img class="ls-s3"  src="example/layerslider/nf8.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : left; slideoutdirection : left;  delayin : 1200; showuntil : 2200; ">
                    <img class="ls-s3"  src="example/layerslider/nf9.png"  style="position: absolute; top: 0px; left: -100px; slidedirection : top; slideoutdirection : left;  delayin : 1300; showuntil : 1900; ">


                </div>

            </div>

        </div>
    </div>
</section>
<!-- end: Slider -->

<!-- start: Container -->
<div class="container">

<div class="row-fluid">

    <!-- start: Page section -->
    <section id="page-sidebar">

	    <!-- live broadcast -->
            <div class="hero-unit">
                <!--<center><iframe width="760" height="515" src="https://www.youtube.com/embed/mJOdmHf8Qaw" frameborder="0" allowfullscreen></iframe></center>-->
								<center>
									<video width="320" height="205" controls>
									  <source src="videos/gardos.mp4" type="video/mp4">
									</video>
									<video width="320" height="205" controls>
									  <source src="videos/paovic.mp4" type="video/mp4">
									</video>
									<video width="320" height="205" controls>
									  <source src="videos/suchanek.mp4" type="video/mp4">
									</video>
								<center>
								<br />
								<center>
									<video width="470" height="305" controls>
									  <source src="videos/ceremonial.mp4" type="video/mp4">
									</video>
									<video width="470" height="305" controls>
									  <source src="videos/karabec-horut.mp4" type="video/mp4">
									</video>
								<center>
            </div>

    	        <!-- start: Wrapper -->
        <div class="wrapper">
	        <h1>Czech Open 2015</h1>
            <!-- Start: Related Projects -->
            <section class="row-fluid portfolio tiled first clearfix">
                <article class="span6">
                    <div class="inner-image"><a href="images/gallery2015/1.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]"><img src="images/gallery2015/1.jpg" alt=""  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" /></a></div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3 metro-tile">
                    <div class="inner-image">
                        <a href="images/gallery2015/2.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/2.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery2015/12.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/12.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery2015/14.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/14.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery2015/19.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/19.jpg" alt="photo" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
            </section>
            <section class="row-fluid portfolio tiled clearfix" style="margin-top: 0;">
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery2015/21.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/21.jpg" alt="photo" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery2015/24.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/24.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery2015/28.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/28.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery2015/29.jpg" data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]">
                            <img src="images/gallery2015/29.jpg" alt="photo"  data-rel="prettyPhoto[gallery]" rel="prettyPhoto[gallery]" />
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2015</a></h4>
                        </div>
                    </div>
                </article>
            </section>

            <hr />
			<!--
	        <h1>Czech Open 2011</h1>
            <section class="row-fluid portfolio tiled first clearfix">
                <article class="span6">
                    <div class="inner-image"><a href="images/gallery/1.jpg" data-rel="prettyPhoto"><img src="images/gallery/1.jpg" alt=""/></a></div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3 metro-tile">
                    <div class="inner-image">
                        <a href="images/gallery/2.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/2.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery/3.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/3.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery/4.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/4.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span3">
                    <div class="inner-image">
                        <a href="images/gallery/7.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/7.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
            </section>
            <section class="row-fluid portfolio tiled clearfix" style="margin-top: 0;">
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/10.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/10.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/22.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/22.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/29.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/29.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/24.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/24.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/42.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/42.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
                <article class="span2">
                    <div class="inner-image">
                        <a href="images/gallery/52.jpg" data-rel="prettyPhoto">
                            <img src="images/gallery/52.jpg" alt="photo"/>
                        </a>
                    </div>
                    <div class="sliding">
                        <div class="inner-text">
                            <h4 class="title"><a href="#">Czech Open 2011</a></h4>
                        </div>
                    </div>
                </article>
            </section>-->
            <!-- end: Related Projects -->

            <!-- live broadcast -->
            <div class="hero-unit">
                <h1 style="font-size:53px" align="center">Online broadcasting of matches</h1>
                <p></p>
                <br />
                <p align="center"><a href="http://ostatni.tvcom.cz/" class="btn btn-primary btn-large"><img src="images/logo-over.png"/></a></p>
            </div>

            <div class="hero-unit">
                <h1 style="font-size:53px">Welcome to Czech Open 2015 in Ostrava!!!</h1>
                <p></p>
                <br />
                <p><a href="forms-and-download.php" class="btn btn-primary btn-large">Register</a></p>
            </div>

            <!-- start: Sponsors -->
            <div class="row-fluid">
                <ul class="thumbnails pull-center">
                    <li class="span4 thumbnail">
                        <div class="image-icon"><a href="http://www.ipttc.org" target="_blank"><img src="images/sponsors/ittf.png" /></a></div>
                    </li>
										<li class="span4 thumbnail">
	                      <div class="image-icon"><a href="http://www.ping-pong.cz" target="_blank"><img src="images/sponsors/cast.png" /></a></div>
	                  </li>
										<li class="span4 thumbnail">
	                      <div class="image-icon"><a href="http://www.tjostrava.cz" target="_blank"><img src="images/sponsors/tj-ostrava.png" /></a></div>
	                  </li>
                    <!--<li class="span4 thumbnail">
                        <div class="image-icon"><a href="http://www.caths.cz" target="_blank"><img src="images/sponsors/caths.png" /></a></div>
                    </li>-->
                    <!--<li class="span4 thumbnail">
                        <div class="image-icon"><a href="http://www.cez.cz" target="_blank"><img src="images/sponsors/cez.png" /></a></div>
                    </li>-->
                </ul>
            </div>

            <div class="row-fluid">
                <ul class="thumbnails pull-center">
									<li class="span4 thumbnail">
											<div class="image-icon"><a href="http://www.kr-moravskoslezsky.cz" target="_blank"><img src="images/sponsors/msk.png" /></a></div>
									</li>
									<li class="span4 thumbnail">
											<div class="image-icon"><a href="http://www.ostrava.cz" target="_blank"><img src="images/sponsors/ostrava.png" /></a></div>
									</li>
                  <li class="span4 thumbnail">
                      <div class="image-icon"><a href="http://www.parkinn.cz/hotel-ostrava" target="_blank"><img src="images/sponsors/park-inn.png" /></a></div>
                  </li>
                </ul>
            </div>
            <!-- end: Sponsors -->

        </div>
        <!-- end: Wrapper -->

    </section>
    <!-- end: Page section -->

</div>

<?
	include('./footer.php')
?>

</div>
<!-- end: Container -->

</body>
</html>
