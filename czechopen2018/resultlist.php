<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?php 
	$current = 'resultlist';
?>

<head>
    <title>Results list ~ <?php  include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?php 
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?php 
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?php 
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?php 
    	include('./menu.php');
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Results list</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Results list</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->
                <div class="row-fluid">
                    <div class="span12">
                    <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF2-3.pdf">SF2-3</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF4.pdf">SF4</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF5.pdf">SF5</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF6.pdf">SF6</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF7.pdf">SF7</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF8.pdf">SF8</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF9.pdf">SF9</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF10.pdf">SF10</a>
                        </p>
						<p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SF11.pdf">SF11</a>
                        </p>
						<p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM1.pdf">SM1</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM2.pdf">SM2</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM3.pdf">SM3</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM4.pdf">SM4</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM5.pdf">SM5</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM6.pdf">SM6</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM7.pdf">SM7</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM8.pdf">SM8</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM9.pdf">SM9</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM10.pdf">SM10</a>
                        </p>
                        <p>
                        	<br />
                        	<div class="fs1" aria-hidden="true" data-icon="&#xe0c4;"></div>
                        	<a href="docs/2018/rozlosovani/SM11.pdf">SM11</a>
                        </p>
                    </div>
                </div>

								<hr />


                </div>
              

                <hr/>

                <!-- live broadcast -->
	            <div class="hero-unit">
	                <h1 style="font-size:53px" align="center">Online broadcasting of matches</h1>
	                <p></p>
	                <br />
	                <p align="center"><a href="http://ostatni.tvcom.cz/" class="btn btn-primary btn-large"><img src="images/logo-over.png"/></a></p>
	            </div>


        </section>
        <!-- end: Page section -->



    </div>

    <!-- start: Footer -->
    <?php 
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
