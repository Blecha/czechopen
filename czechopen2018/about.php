<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<head>
    <title>About Us</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?php 
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?php 
	                include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?php 
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?php 
    	include('./menu.php');		
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>About Us</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; About Us</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->

                <div class="row-fluid portfolio portfolio-extended member-info">
                    <article class="span3">
                        <div class="inner-image">
                            <img src="example/team1.jpg" alt=""/>
                            <span class="frame-overlay"></span>
                        </div>
                        <div class="inner-text">
                            <h3 class="member-name">Jhon Doe</h3>
                            <p>Per insolens urbanitas pris tantas corpora cum commune dignissim pro delectus salutatus ever labitur.</p>
                            <p class="member-social">
                                <a href="#"><i class="icon-facebook-sign"></i></a>
                                <a href="#"><i class="icon-twitter-sign"></i></a>
                                <a href="#"><i class="icon-envelope"></i></a>
                            </p>
                        </div>
                    </article>
                    <article class="span3">
                        <div class="inner-image">
                            <img src="example/team2.jpg" alt=""/>
                            <span class="frame-overlay"></span>
                        </div>
                        <div class="inner-text">
                            <h3 class="member-name">Jhon Doe</h3>
                            <p>Per insolens urbanitas pris tantas corpora cum commune dignissim pro delectus salutatus ever labitur.</p>
                            <p class="member-social">
                                <a href="#"><i class="icon-facebook-sign"></i></a>
                                <a href="#"><i class="icon-twitter-sign"></i></a>
                                <a href="#"><i class="icon-envelope"></i></a>
                            </p>
                        </div>
                    </article>
                    <article class="span3">
                        <div class="inner-image">
                            <img src="example/team3.jpg" alt=""/>
                            <span class="frame-overlay"></span>
                        </div>
                        <div class="inner-text">
                            <h3 class="member-name">Jhon Doe</h3>
                            <p>Per insolens urbanitas pris tantas corpora cum commune dignissim pro delectus salutatus ever labitur.</p>
                            <p class="member-social">
                                <a href="#"><i class="icon-facebook-sign"></i></a>
                                <a href="#"><i class="icon-twitter-sign"></i></a>
                                <a href="#"><i class="icon-envelope"></i></a>
                            </p>
                        </div>
                    </article>
                    <article class="span3">
                        <div class="inner-image">
                            <img src="example/team4.jpg" alt=""/>
                            <span class="frame-overlay"></span>
                        </div>
                        <div class="inner-text">
                            <h3 class="member-name">Jhon Doe</h3>
                            <p>Per insolens urbanitas pris tantas corpora cum commune dignissim pro delectus salutatus ever labitur.</p>
                            <p class="member-social">
                                <a href="#"><i class="icon-facebook-sign"></i></a>
                                <a href="#"><i class="icon-twitter-sign"></i></a>
                                <a href="#"><i class="icon-envelope"></i></a>
                            </p>
                        </div>
                    </article>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="hero-unit">
                            <blockquote>
                                <p>Est audiam urbanitas omittantur an, eum suas tota delectus an. Cu pro graece aliquando abhorreant. An usu sensibus disputationi consectetuer. His ridens tamquam prodesset id. Duo probo dicunt efficiendi in, cu vim graecis invidunt, nemore patrioque pri id. Mei aliquip menandri ea. Ex pri dictas petentium constituto, cum cu facilis blandit. Dicat summo duo ea, ex sed assum delicatissimi. Has pertinax pericula accommodare ei, diam facer deterruisset eum et. </p>
                                <small>Jhon Doe</small>
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span4">
                        <div class="well">
                            <h3>Our Philosophy</h3>
                            <p>Sanctus sea sed takimata ut vero voluptua. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="well">
                            <h3>Our Mission</h3>
                            <p>Sanctus sea sed takimata ut vero voluptua. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="well">
                            <h3>Web Development</h3>
                            <p>Sanctus sea sed takimata ut vero voluptua. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <?php 
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
