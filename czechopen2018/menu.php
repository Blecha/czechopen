<div class="container">
        <nav id="menu" class="clearfix">
            <ul>
                <li><a href="./index.php" <?php  if($current == 'index'){ ?>class="current"><?php  } ?><span class="name">Home Page</span></a></li>
                <li><a href="./information.php" <?php  if($current == 'information'){ ?>class="current"><?php  } ?><span class="name">Information</span></a></li>
                <!-- <li><a href="./technical-information.php" <?php  if($current == 'technical'){ ?>class="current"><?php  } ?><span class="name">Technical Information</span></a></li> -->
                <li style="max-width: 140px !important; word-wrap: break-word;"><a href="./forms-and-download.php" <?php  if($current == 'forms'){ ?>class="current"><?php  } ?><span class="name"><center><p style="margin-top: 0px;">Forms and download</p></center></span></a></li>
                <li><a href="./contact.php" <?php  if($current == 'contact'){ ?>class="current"><?php  } ?><span class="name">Contact</span></a></li>
                <li><a href="./resultlist.php" <?php  if($current == 'resultlist'){ ?>class="current"><?php  } ?><span class="name"><center>Results</span></center></a></li>
                <li><a href="./playerlist.php" <?php  if($current == 'playerlist'){ ?>class="current"><?php  } ?><span class="name"><center>Players</span></center></a></li>
                <li><a href="./umpireslist.php" <?php  if($current == 'umpireslist'){ ?>class="current"><?php  } ?><span class="name"><center>Umpires</span></center></a></li>
                <li><a href="./gallery.php" <?php  if($current == 'gallery'){ ?>class="current"><?php  } ?><span class="name">Gallery 2018</span></a></li>
                <li><a href="./sponsors.php" <?php  if($current == 'sponsors'){ ?>class="current"><?php  } ?><span class="name">Partners</span></a></li>
                <li><a href="./czech-open-2017/" <?php  if($current == 'czechopen2017'){ ?>class="current"><?php  } ?><span class="name"><center>Archiv 2017</span></center></a></li>
            </ul>
        </nav>
    </div>
