<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<?php 
	$current = 'information';
?>

<head>
    <title>Information ~ <?php  include('title.php')?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-icomoon.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/font-awesome-ie7.css"/>
    <![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.quicksand.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.ui.totop.js"></script>
    <script type="text/javascript" src="js/ajax-mail.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- start: Top Menu -->
<section id="top-menu">
    <div class="container">
        <div class="row">
            <div class="span9 logo" style="padding-top:15px">
                <?php 
	                include('./logo.php')
                ?>
            </div>
            <div class="span2" style="padding-top:30px">
                <?php 
	                //include('./main-sponsor.php')
                ?>
            </div>
            <div class="span1">
                <?php 
                	include('./social.php')
                ?>
            </div>
        </div>
    </div>
</section>
<!-- start: Top Menu -->

<!-- start: Header -->
<header id="header">
    <!-- start: Main menu -->
    <?php 
    	include('./menu.php');		
    ?>
    <!-- end: Main menu -->
</header>
<!-- end: Header -->

<!-- start: Container -->
<div class="container">

    <div class="row-fluid">

        <!-- start: Page section -->
        <section id="page-sidebar" class="pull-left span12">

            <!-- start: Wrapper -->
            <div class="wrapper">

                <!-- start: Page header / Breadcrumbs -->
                <section class="breadcrumbs">
                    <div class="table">
                        <div class="page-header table-cell">
                            <h1>Information</h1>
                        </div>
                        <div class="breadcrumbs table-cell">
                            <div>You are here: <a href="#">Home</a> &nbsp;&rsaquo;&nbsp; Information</div>
                        </div>
                    </div>
                </section>
                <!-- end: Page header / Breadcrumbs -->

                <div class="row-fluid pull-center">
                    <div class="span12 service">
                        <div class="hero-unit">
                            <h2>Competition venue</h2>
                            <p>
	                            Telovychovna jednota Ostrava, Varenska street 3098/40a, 702 00 Ostrava, Czech Republic
                            </p>
                            <div class="gmap"><iframe class="" width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=cs&amp;geocode=&amp;q=TJ+Ostrava,+Varensk%C3%A1+40+a,+702+00+Ostrava&amp;sll=37.0625,-95.677068&amp;sspn=50.244827,93.076172&amp;t=m&amp;ie=UTF8&amp;hq=TJ&amp;hnear=Varensk%C3%A1+3050%2F40,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;ll=49.835054,18.269201&amp;spn=0.006295,0.006295&amp;output=embed"></iframe>
                        </div>
                        	<p>    
								Sports hall of TJ Ostrava about 1200 m from the hotel
<br /><br />

							<strong>OFFICIAL HOTEL</strong><br /> 
							The hotel for teams is:
							<br />
							<center>	 
							<table>
								<tr>
									<td>Name:</td><td>Park Inn by Radisson Ostrava - 4 stars</td>
								</tr>
								<tr>
									<td>Physical/ postal address:&nbsp;&nbsp;&nbsp;</td><td>Hornopolni street 3313/42, Ostrava</td>
								</tr>
								<tr>
									<td>E-mail:</td><td><a href="mailto:rezervace.ostrava@rezidoparkinn.com">rezervace.ostrava@rezidoparkinn.com</a></td>
								</tr>
								<tr>
									<td>Phone:</td><td>+420 595 195 509</td>
								</tr>
								<tr>
									<td>Fax:</td><td>+420 595 195 555</td>
								</tr>
								<tr>
									<td>Website address:</td><td><a href="http://www.parkinn.cz/hotel-ostrava" target="_blank">www.parkinn.com/hotel-ostrava</a></td>
								</tr>
							</table>
							</center>	
							<br />
							<div class="gmap"><iframe class="2" width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=cs&amp;q=Park+Inn+Ostrava+Hornopoln%C3%AD+3313%2F42,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;ie=UTF8&amp;geocode=FUmI-AIdw8EWAQ&amp;split=0&amp;hq=&amp;hnear=Park+Inn+Ostrava+Hornopoln%C3%AD+3313%2F42,+702+00+Ostrava,+%C4%8Cesk%C3%A1+republika&amp;t=m&amp;z=14&amp;ll=49.842249,18.268611&amp;output=embed"></iframe>
							</div>
                            </p>
                            
                            <p>    
							Sports hall of TJ Ostrava about 1200 m from the hotel
<br /><br />

							<strong>Way to the hotel</strong>
							<br /> 
							<br />
							<div class="gmap"><iframe class="2" width="100%" height="650" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=Varensk%C3%A1+40a,+702+00+Moravsk%C3%A1+Ostrava&amp;daddr=Hornopolni+street+3313%2F42,+Ostrava&amp;hl=cs&amp;geocode=FfBs-AId1sIWASltIJ7RFeMTRzHu1kwdQmip6A%3BFUmI-AIdxMEWASnTIysfGuMTRzE_g8_FwVO5Ow&amp;aq=&amp;sll=49.835248,18.268886&amp;sspn=0.010075,0.022724&amp;mra=ls&amp;ie=UTF8&amp;ll=49.835242,18.26889&amp;spn=0.008122,0.007193&amp;t=m&amp;output=embed"></iframe>
							</div>
                            </p>

                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row-fluid">
                    <div class="span12 service">
                        <div class="well">
                            <h2>TRANSPORTATION</h2>
                            <div class="service-desc">
								<p>
									The organizers will provide transportation for teams and officials from the airport Ostrava, or from the Main Railway stations in Ostrava to hotel Park Inn and back and local transport from hotel to Venue and back. We will use for it the right number of buses and minibuses.
								</p>
								<p>
	Near Ostrava is also Leos Janacek Airport Ostrava serving domestic and international destinations (Prague, London, Paris, etc.). Ostrava has direct train connection with the capital Prague and other cities abroad  (Wien, Warszawa, Bratislava, Kosice, etc.). There are two main railway stations: Ostrava-Svinov (recommended) and Ostrava-Hlavni nadrazi (old). 

								</p>									
								<img src="images/czechopen_map_airports.jpg" />
								<br /><br />
								<p><strong>Free pick up/drop off transport service</strong></p>

<p>From the airport in Ostrava and the railway station Ostrava-Svinov the organizer provides transport to/from the hotel for free.</p>

<p><strong>Ostrava: Leoš Janáček Ostrava Airport</strong>, Czech Republic</p>

<p>Distance: 25 km – Bus (45 min)</p>

<p>Web: <a href="http://www.airport-ostrava.cz/en/">http://www.airport-ostrava.cz/en/</a></p>

<br />

<p><strong>Ostrava-Svinov Railway Station</strong>, Czech Republic</p>

<p>Distance: 5 km – Bus (15 min)</p>

<br />

<p><strong>Other destination possibilities</strong></p>

<p>There is also possibility to arrive from many farther airports in Katowice (POL), Krakow (POL), Brno (CZE), Vienna (AUT), Bratislava (SVK) and Prague (CZE). </p>

<p>From the following other airports organizer does not provide transportation (in exceptional cases only – by appointment).</p>

<br />

<p><strong>Katowice:</strong> Katowice International Airport, Poland</p>

<p>Distance: 120 km – Bus (1,5 h)</p>

<p>Web: <a href="https://www.katowice-airport.com/en/">https://www.katowice-airport.com/en/</a></p>

<p>Operated by: LEO Express</p>

<br />

<p><strong>Krakow:</strong> John Paul II International Airport Kraków-Balice, Poland</p>

<p>Distance: 150 km – Bus (2 h)</p>

<p>Web: <a href="http://www.krakowairport.pl/en/">http://www.krakowairport.pl/en/</a> </p>

<p>Operated by: LEO Express, RegioJet</p>

<br />

<p><strong>Vienna:</strong> Vienna International Airport, Austria</p>

<p>Distance: 310 km – Bus (3 h), Train (4 h)</p>

<p>Web: <a href="http://www.viennaairport.com/en">http://www.viennaairport.com/en</a></p>

<p>Operated by: LEO Express, ČD Czech Railways</p>

<br />

<p><strong>Prague:</strong> Vaclav Havel Airport Prague, Czech Republic</p>

<p>Distance: 390 km – Train (3 - 3,5 h)</p>

<p>Web: <a href="http://www.prg.aero/en/">http://www.prg.aero/en/</a></p>

<p>Operated by: LEO Express, RegioJet, ČD Czech Railways</p>

<br />

<p>Journey time is about 30 min from airport to railway station in Prague and 3 hours by train (from Prague to Ostrava).</p>

<p><strong>Brno:</strong> Brno-Tuřany Airport, Czech Republic</p>

<p>Distance: 170 km – Bus (2 h), Train (3 h)</p>

<p>Web: <a href="http://www.brno-airport.cz/en/">http://www.brno-airport.cz/en/</a></p>

<p>Operated by: ČD Czech Railways</p>

<br />

<p><strong>Wroclaw</strong>: Copernicus Airport, Poland</p>

<p>Distance: 250 km – Bus (2,5 h), Train (5,5 h)</p>

<p>Web: <a href="http://airport.wroclaw.pl/en/">http://airport.wroclaw.pl/en/</a></p>

<p>Operated by: LEO Express</p>

<br />

<p><strong>Bratislava:</strong> Letisko M.R.Štefánika - Airport Bratislava, Slovakia</p>

<p>Distance: 305 km – Train (3,5 h)</p>

<p>Web: <a href="https://www.bts.aero/en/">https://www.bts.aero/en/</a></p>

<p>Operated by: ČD Czech Railways</p>

<br />

<p><strong>Warsaw:</strong> Warsaw Chopin Airport, Poland</p>

<p>Distance: 390 km – Train (5 h)</p>

<p>Web: <a href="https://www.lotnisko-chopina.pl/en/index.html">https://www.lotnisko-chopina.pl/en/index.html</a></p>

<p>Operated by: ČD Czech Railways, LEO Express</p>

<br />

<p><strong>Travel companies contacts</strong></p>

<br />

<p>LEO Express – <a href="http://www.le.cz">http://www.le.cz</a> </p>

<p>ČD Czech Railways – <a href="http://www.cd.cz">http://www.cd.cz</a> </p>

<p>RegioJet – <a href="https://www.regiojet.com/en">https://www.regiojet.com/en</a></p>
                            </div>
                        </div>
                    </div>                    
                </div>

                <hr/>
                
                <div class="row-fluid">					
                    <div class="span12 service">
                        <div class="well">
                            <h2>ENTRY FEES</h2>
                            <div class="service-icon"><i class="icon-cog"></i></div>
                            <div class="service-desc">
                                <p>For PTT events, the entry fees include accommodation and capitation fees. The entry fees have to be paid in Euro to the organizers as set out below:</p>

								<br />

								<img src="images/fees-2018.png">

								<br />
								<br />

								<p><strong>Entry fee:</strong> </p>

								<p><strong>€ 590</strong>/person/double room (ITTF Capitation fee included)</p>

								<p><strong>€ 690</strong>/person/single room (ITTF Capitation fee included)</p>

								<p><strong>Entry fee to be submitted with the 1st Entry (10th June):</strong> </p>

								<p><strong>€ 200</strong>/person in double room</p>

								<p><strong>€ 200</strong>/person in single room</p>

								<p><strong>Entry fee to be submitted with the 2nd Entry (10th August):</strong> </p>

								<p><strong>€ 390</strong>/person in double room</p>

								<p><strong>€ 490</strong>/person in single room</p>

								<p><strong>Special price for a class 1 and 11 player´s guide:</strong> (€ 465 entry fee+€ 25 capitation fee) € 490 per person sharing with a class 1/11 player.</p>

								<p><strong>Note:</strong> The costs of the classification day is not included in the entry fee.</p>

								<p><strong>Note:</strong> The first official meal is the dinner on the day of arrival and the last official meal is the breakfast on the day of departure. If you would like to have a lunch on your arrival or departure day, you need to pay additional cost.</p>

								<p>Should there be any additional team members or for the team member’s <strong>additional nights</strong>, the fee is <strong>€ 80</strong>/person/double room or <strong>€ 110</strong>/person/single room.</p>

								<p>If an association wishes only to pay an <strong>accreditation fee</strong>, it will cost <strong>€ 250</strong>/person but does not include any transport, meals or accommodation which should be organized by the respective association. In this case an accreditation fee must be paid in the first entry! </p>
                                <table>
                                	<tr>
                                		<td>Name of organisation:</td><td><strong>TJ Ostrava</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Bank name:</td><td><strong>Komercni banka a.s.</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Account name:</td><td><strong>TJ Ostrava</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Account number:</td><td><strong>61830761/0100</strong></td>
                                	</tr>
                                	<tr>
                                		<td>SWIFT:</td><td><strong>KOMBCZPP</strong></td>
                                	</tr>
                                	<tr>
                                		<td>IBAN:</td><td><strong>CZ8201000000000061830761</strong></td>
                                	</tr>
                                	<tr>
                                		<td>Specification for Payment:</td><td><strong>“Czech Open 2018”</strong></td>
                                	</tr>
                                </table>
                                <h4 style="color:red;">PLEASE,  NOTE THAT BANK CHARGES MUST BE  INCLUDED IN THE ENTRY FEES !</h4>
                            </div>
                        </div>
                    </div>

					<div class="row-fluid">
						<div class="span6 service">
                        <div class="well">
                            <h2>VISA</h2>
                            <div class="service-icon"><i class="icon-cogs"></i></div>
                            <div class="service-desc">
                                <p>
                                    Should you need assistance to apply for a visa (e.g. a letter of invitation), please provide the organisers with the following details:
                                </p>
                                <ul class="the-icons">
                                    <li><i class="iconm-arrow-right-7"></i> Full name as in passport</li>
                                    <li><i class="iconm-arrow-right-7"></i> Function in the team</li>
                                    <li><i class="iconm-arrow-right-7"></i> Passport number</li>
                                    <li><i class="iconm-arrow-right-7"></i> Passport expiry date</li>
                                </ul>
                                <p>
	                                Note: the requirements for visas are not under the control of the ITTF or the organisers but under the Slovenian Government’s jurisdiction and the association must fulfil all requirements in order to get a visa in time. It may take more than a month to obtain a visa.
                                </p>
								<br /><br /><br /><br /><br /><br /><br /><br /><br />
                            </div>
                        </div>
                    </div>
						<div class="span6 service">
							<div class="well">
								<h2>CANCELLATION POLICY</h2>
								<div class="service-icon"><i class="icon-cogs"></i></div>
									<div class="service-desc">
										<p>
											The policy applies as follows:<br /><br />
											25.1	cancellation after the first entry but before the second entry:  the first entry fee is forfeited.<br /><br />
											25.2	cancellations after the second entry:  the first entry fee plus an additional 30% of the entry fee is forfeited i.e. a total of 60% of the total entry fee.<br /><br />
											25.3	cancellations within 10 calendar days of the arrival date will be decided by the organisers in consultation with the TD.<br />
											<br />
											This is provided that the player is not able to prove circumstances beyond his or her control e.g. admission to hospital.
											<br />
											<br />
											<br />
											<br />
											<br />
											<br />
											<br />
											<br />
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				
                <hr/>                

            </div>
            <!-- end: Wrapper -->

        </section>
        <!-- end: Page section -->

    </div>

    <!-- start: Footer -->
    <?php 
	include('./footer.php')
?>
    <!-- end: Footer -->

</div>
<!-- end: Container -->

</body>
</html>
